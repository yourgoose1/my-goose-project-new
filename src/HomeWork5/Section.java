package HomeWork5;

public enum Section {
    A1("A1",20, "Пройдите по пути из семечек. Приятного матча!"),
    A2("A2",20, "Идите по пути из семечек, но держитесь правее. Приятного матча!"),
    A3("A3",20, "Развернитесь на 360 градусов и бегите в сторону голубей. Приятного матча!"),
    B1("B1",30, "Пройдите прямо, до спящего охранника. После чего произведите сальто с места. Приятного матча!"),
    B2("B2",40, "Пожалуйста,  возьмите поводок от вороны в руку. Она довеет вас в нужное метсо. Приятного матча!"),
    B3("B3",50, "Пройдите прямо 10 шагов, поверните направо и пройдите 10 шагов. Сюда вам не надо. А ваше место левее. Приятного матча!"),
    VIP("VIP",10, "Пожалуйста, наденьте бахилы и присаживайтесь в бизнес-такси. Мы вас подвезем к вашему месту. Приятного матча, господин!");

    Section(String nameOfSection, int numberOfSeats, String howToGetThere) {
        this.nameOfSection = nameOfSection;
        this.numberOfSeats = numberOfSeats;
        this.howToGetThere = howToGetThere;
    }

    @Override
    public String toString() {
        return "Section{" +
                "nameOfSection='" + nameOfSection + '\'' +
                ", numberOfSeats=" + numberOfSeats +
                ", howToGetThere='" + howToGetThere + '\'' +
                '}';
    }

    public String nameOfSection;
    int numberOfSeats;
    String howToGetThere;


}

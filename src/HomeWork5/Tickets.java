package HomeWork5;

public class Tickets {

    String fullName;
    int ticketNumber;
    String section;
    int seatNumber;

    public Tickets(String fullName, int ticketNumber, String section, int seatNumber) {
        this.fullName = fullName;
        this.ticketNumber = ticketNumber;
        this.section = section;
        this.seatNumber = seatNumber;
    }

        public boolean nameValidation() {
        if (!fullName.isEmpty()) {
            return true;
        } else {
            System.out.println("Ваш билет не действителен!");
        }
            return false;
    }

    public boolean sectionValidation() {
        if (section.equals(Section.A1.nameOfSection)) {
            System.out.println("Сохраните билет до конца матча");
            System.out.println(Section.A1.howToGetThere);
            return true;
        } else if
        (section.equals(Section.A2.nameOfSection)) {
            System.out.println("Сохраните билет до конца матча");
            System.out.println(Section.A2.howToGetThere);
            return true;
        } else if
        (section.equals(Section.A3.nameOfSection)) {
            System.out.println(Section.A3.howToGetThere);
            return true;
        } else if
        (section.equals(Section.B1.nameOfSection)) {
            System.out.println(Section.B1.howToGetThere);
            return true;
        } else if
        (section.equals(Section.B2.nameOfSection)) {
            System.out.println(Section.B2.howToGetThere);
            return true;
        } else if
        (section.equals(Section.B3.nameOfSection)) {
            System.out.println(Section.B3.howToGetThere);
            return true;
        } else if
        (section.equals(Section.VIP.nameOfSection)) {
            System.out.println("С прибытием " + fullName + "!");
            System.out.println(Section.VIP.howToGetThere);
            return true;
        } else
            return false;
    }
}



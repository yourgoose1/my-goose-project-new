package HomeWork5;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class ValidationTerminal {
    public static void main(String[] args) {
        // массив с именами для рандома
        String[] fullNames = {
                "Зубенко Михаил Петрович",
                "Оби Ван Кеноби",
                "",
                "Федор Михайлович Достоевский",
                "Бонд Джеймс Бонд"};
        Collections.shuffle(Arrays.asList(fullNames));

        // рандом для ticketNumber
        Random random = new Random();

        // рандом для секции
        String[] sections = {
                Section.A1.nameOfSection,
                Section.A2.nameOfSection,
                Section.A3.nameOfSection,
                Section.B1.nameOfSection,
                Section.B2.nameOfSection,
                Section.B3.nameOfSection,
                Section.VIP.nameOfSection,
                "B52"
        };
        Collections.shuffle(Arrays.asList(sections));

        // рандом для номера места
        Random random1 = new Random();

        Tickets[] tickets = new Tickets[5];
        for (int i =0;i<5;i++) {
            tickets[i] = new Tickets(fullNames[new Random().nextInt(fullNames.length)],
                    random.nextInt(10000),
                    sections[new Random().nextInt(sections.length)],
                    random1.nextInt(191));
        }
        for (Tickets ticket: tickets) {
            System.out.println("Здравствуйте. Происходит проверка корректности вашего билета");
            if (ticket.nameValidation() && ticket.sectionValidation())
            {
                System.out.println("Билет корректен!");
            }
            else {
                System.out.println("Билет не соответствует шаблону! Позвоните на горячую линию: 8-800-555-35-35!");
            }
        }
    }
}

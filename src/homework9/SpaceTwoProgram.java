package homework9;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SpaceTwoProgram {
    public static void main(String[] args) {
        Human[] humans = new Human[10];
        humans[0] = new Human(123,"Morgan",34);
        humans[1] = new Human(137,"Sam",25);
        humans[2] = new Human(404,"Dobby",19);
        humans[3] = new Human(500,"Tom",80);
        humans[4] = new Human(199,"Vasya",44);
        humans[5] = new Human(1954,"Vasya",48);
        humans[6] = new Human(89,"Paul",18);
        humans[7] = new Human(776,"Misha",64);
        humans[8] = new Human(77,"Baragozin",33);
        humans[9] = new Human(1,"Luntik",15);

        List<Human> humansList = Arrays.asList(humans);
        humansList.stream()
                .limit(8)
                .sorted((o1, o2) -> o1.age-o2.age)
                .filter(Human -> Human.age >= 20)
                .filter(Human -> Human.age <= 45)
                .peek(System.out::println)
                .map(human -> {
                    Astronaut astronaut = new Astronaut();
                    astronaut.name = human.name;
                    astronaut.age = human.age;
                    astronaut.startPlanet = "Earth";
                    return astronaut;
                })
                .sorted(Comparator.comparing(astronaut -> astronaut.name))
                .toList()
        .forEach(System.out::println);
    }
}

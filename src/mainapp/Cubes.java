package mainapp;
import gameutils.RollTheCube;

import static gameutils.RollTheCube.rollTheFourCube;

public class Cubes {
    public static void main(String[] args) {
        RollTheCube.rollTheFourCube();
        RollTheCube.rollTheSixCube();
        RollTheCube.rollTheTwelveCube();
        RollTheCube.rollTheTwentyCube();
        RollTheCube.rollTheTenCube();
        RollTheCube.rollTheNextTenCube();
    }
}

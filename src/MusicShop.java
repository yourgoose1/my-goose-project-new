import java.lang.reflect.Array;
import java.util.Arrays;

public class MusicShop {
   String address;
    private static VinylRecord[] vinylStorage = new VinylRecord[3];
    static int index = 0;

    public static void showVinylStorage() {
        System.out.println(Arrays.toString(vinylStorage));
    }

    public static void addVinylToStorage (VinylRecord vinylRecord) {
        vinylStorage[index] = vinylRecord;
        index++;
    }
}

package homework7;

import java.util.LinkedList;

public class TimeSpendDemoLinkedList {
    public static void main(String[] args) {
        LinkedList<String> linkedList = new LinkedList<>();
        for (int i = 0; i < 1000000; i++) {
            linkedList.add("Элемент" + i);
        }
        long timeBeforeAddLinkedList = System.currentTimeMillis();
        addToStartLinkedList(linkedList);
        long timeAfterAddLinkedList = System.currentTimeMillis();
        long timeBeforePrintFromMiddleLinkedList = System.currentTimeMillis();
        printFromMiddleLinkedList(linkedList);
        long timeAfterPrintFromMiddleLinkedList = System.currentTimeMillis();
        long timeBeforeRemoveFromLinkedList = System.currentTimeMillis();
        removeFromLinkedList(linkedList);
        long timeAfterRemoveFromLinkedList = System.currentTimeMillis();
        long timeBeforeReplaceFromLinkedList = System.currentTimeMillis();
        replaceFromLinkedList(linkedList);
        long timeAfterReplaceFromLinkedList = System.currentTimeMillis();
        System.out.printf("Время выполнения добавления в linkedList: %d мс\n",
                timeAfterAddLinkedList-timeBeforeAddLinkedList);
        System.out.printf("Время выполнения вывода в linkedList: %d мс\n",
                timeAfterPrintFromMiddleLinkedList-timeBeforePrintFromMiddleLinkedList);
        System.out.printf("Время выполнения удаления в linkedList: %d мс\n",
                timeAfterRemoveFromLinkedList-timeBeforeRemoveFromLinkedList);
        System.out.printf("Время выполнения замены в linkedList: %d мс\n",
                timeAfterReplaceFromLinkedList-timeBeforeReplaceFromLinkedList);
    }

    public static void addToStartLinkedList(LinkedList<String> linkedList) {
        linkedList.add(0, "Первый элемент");
    }

    public static void printFromMiddleLinkedList(LinkedList<String> linkedList) {
        for (int i = 500000; i <= 500100; i++) {
            System.out.println(i);
        }
    }

    public static void removeFromLinkedList(LinkedList<String> linkedList) {
        for (int i = 0; i <= 1000; i++) {
            linkedList.remove(i);
        }
    }

    public static void replaceFromLinkedList(LinkedList<String> linkedList) {
        for (int i = 500000; i <= 500100; i++) {
            linkedList.set(i, "Измененный элемент");
        }
    }
}

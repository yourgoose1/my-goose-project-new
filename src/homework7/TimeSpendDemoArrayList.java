package homework7;

import java.util.ArrayList;

public class TimeSpendDemoArrayList {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            arrayList.add("Элемент" + i);
        }
        long timeBeforeAddArrays = System.currentTimeMillis();
        addToStartArrays(arrayList);
        long timeAfterAddArrays = System.currentTimeMillis();
        long timeBeforePrintFromMiddleArrays = System.currentTimeMillis();
        printFromMiddleArrays(arrayList);
        long timeAfterPrintFromMiddleArrays = System.currentTimeMillis();
        long timeBeforeRemoveFromArrays = System.currentTimeMillis();
        removeFromArrayList(arrayList);
        long timeAfterRemoveFromArrays = System.currentTimeMillis();
        long timeBeforeReplaceFromArrays = System.currentTimeMillis();
        replaceFromarrayList(arrayList);
        long timeAfterReplaceFromArrays = System.currentTimeMillis();
        System.out.printf("Время выполнения добавления в ArrayList: %d мс\n",
                timeAfterAddArrays-timeBeforeAddArrays);
        System.out.printf("Время выполнения вывода в ArrayList: %d мс\n",
                timeAfterPrintFromMiddleArrays-timeBeforePrintFromMiddleArrays);
        System.out.printf("Время выполнения удаления в ArrayList: %d мс\n",
                timeAfterRemoveFromArrays-timeBeforeRemoveFromArrays);
        System.out.printf("Время выполнения замены в ArrayList: %d мс\n",
                timeAfterReplaceFromArrays-timeBeforeReplaceFromArrays);
    }

    public static void addToStartArrays(ArrayList<String> arrayList) {
        arrayList.add(0, "Первый элемент");
    }

    public static void printFromMiddleArrays(ArrayList<String> arrayList) {
        for (int i = 500000; i <= 500100; i++) {
            System.out.println(i);
        }
    }

    public static void removeFromArrayList(ArrayList<String> arrayList) {
        for (int i = 0; i <= 1000; i++) {
            arrayList.remove(i);
        }
    }

    public static void replaceFromarrayList(ArrayList<String> arrayList) {
        for (int i = 500000; i <= 500100; i++) {
            arrayList.set(i, "Измененный элемент");
        }
    }
}


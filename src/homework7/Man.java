package homework7;

public class Man {
//    Задание 7.2

     static int numberOfArms;
     Boolean doesHeChushpan;
     String name;
     static String district;

    public Man(int numberOfArms, Boolean doesHeChushpan, String name, String district) {
        Man.numberOfArms = numberOfArms;
        this.doesHeChushpan = doesHeChushpan;
        this.name = name;
        Man.district = district;
    }
    @Override
    public boolean equals(Object m){
        if (m==null) return false;
        Man another = (Man) m;
        return this.name.equals(another.name) && this.doesHeChushpan.equals(another.doesHeChushpan);
    }

    public int hashcode(){
        if (this.doesHeChushpan = true)
            return 4 + this.name.length();
        else
        return 5 + this.name.length();
    }

    public static void main(String[] args) {
        Man firstMan = new Man( 2, true, "Колян", "Машмет");
        Man secondMan = new Man( 2, true, "Колян", "Машмет");
        Man thirdMan = new Man( 2, false, "Володя Перфоратор", "Машмет");
        System.out.println(firstMan.hashcode());
        System.out.println(secondMan.hashcode());
        System.out.println(thirdMan.hashcode());
        System.out.println(firstMan.equals(secondMan));
        System.out.println(firstMan.equals(thirdMan));
        System.out.println(secondMan.equals(thirdMan));

    }
}

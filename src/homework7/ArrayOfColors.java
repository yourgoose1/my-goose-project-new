package homework7;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ArrayOfColors {
    public static void main(String[] args) {

        String[] colors =
                {
                        "красный",
                        "оранжевый",
                        "желтый",
                        "зеленый",
                        "голубой",
                        "синий",
                        "фиолетовый",
                };
        // создали массив цветов

        List<String> colorsList = Arrays.asList(colors);
        System.out.println(colorsList);
        System.out.println("-----------------------");

        //    массив в список

        Iterator<String> iterator = colorsList.iterator();
        while (iterator.hasNext()) {
            String allColors = iterator.next();
            System.out.println(allColors);
        }
        System.out.println("-----------------------");
        //вывели список итератором

        Collections.sort(colorsList);
        for(String abc: colorsList){
            System.out.println(abc);
        }
        System.out.println("-----------------------");
        // вывели в алфавитном порядке

        String position1 = colorsList.get(0);
        String position3 = colorsList.get(2);
        String position5 = colorsList.get(4);
        colorsList.set(0, position1 + " Dark");
        colorsList.set(2, position3 + " Dark");
        colorsList.set(4, position5 + " Dark");
        System.out.println(colorsList);
        System.out.println("-----------------------");
        //    замена 1, 3 и 5 позиции

        for (String forEachColorsList : colorsList) {
            System.out.println(forEachColorsList);
        }
        System.out.println("-----------------------");
        //        вывели список циклом for-each

         System.out.println(colorsList.subList(0, 5));
         System.out.println("-----------------------");
        //         Получить под-список с 1 по 5 элементы включительно

         Collections.swap(colorsList, 0, 3);
        System.out.println(colorsList);
        System.out.println("-----------------------");

        //        метод, который поменяет местами 1 и 4 элементы межды собой.

        String a1 = colorsList.get(3);

        System.out.println(colorsList.contains(a1));
        //Получить элемент с индексом 3, сохранить в переменную а1.
        //Проверить, содержится ли объект из переменной а1 в коллекции.
        System.out.println("-----------------------");

        while (iterator.hasNext()) {
            String color = iterator.next();
            if (color.contains("o")) {
                iterator.remove();
            }
        }
            Object[] arrayOfColor = colorsList.toArray();
            for (Object color : arrayOfColor) {
                System.out.println(color);
        }
        System.out.println("-----------------------");

            String newArray[] = colorsList.toArray(new String[0]);
            for(String arrayOfColors : newArray) {
                System.out.println(arrayOfColors);
            }


    }
}
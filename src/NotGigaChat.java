import org.w3c.dom.ls.LSOutput;

import java.util.Random;
import java.util.Scanner;

public class NotGigaChat {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        String[] answersOnQuestions =
                {
                        "Это мы не проходили",
                        "Это нам не задавали",
                        "Я не в курсе",
                        "Не знаю, я не в ресурсе",
                        "Опять ты со своими вопросами",
                        "Да че ты пристал? Я хз"
                };
        String[] anotherAnswers =
                {
                        "Принял-понял",
                        "Окец-пельмец",
                        "ХА! Прикол",
                        "Боже, что я только что прочитал?",
                        "Купил мужик шляпу, а она ему как раз! А потом, как два! Смешно?",
                        "Не понимаю о чем ты. Может лучше по пиву?"
                };

        while (true) {
            String userEnter = scanner.nextLine();
            int randomAnswersOnQuestions = random.nextInt(answersOnQuestions.length);
            int randomAnotherAnswers = random.nextInt(anotherAnswers.length);

            if (userEnter.endsWith("?")) {
                System.out.println(answersOnQuestions[randomAnswersOnQuestions]);
            }
            else if (userEnter.length() == 0) {
                System.out.println("А чего мы ничего не пишем? Обиделся?");
            }
            else if (userEnter.equalsIgnoreCase("выход")) {
                System.out.println("Пока! Мне было не очень то интересно");
                break;
            }
            else {
                System.out.println(anotherAnswers[randomAnotherAnswers]);
            }
        }
    }
    }



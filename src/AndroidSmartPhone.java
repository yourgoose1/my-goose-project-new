public class AndroidSmartPhone extends SmartPhone{
    String androidVersion;
    Boolean fingerprintAvailible;

    public void unlockPhoneMessage() {
        if (fingerprintAvailible==true) {
            System.out.println("Приложите палец");
        }
        else {
            System.out.println("Введите код");
        }
    }

    public void setupLargeApp() {
        if (ram<=4) {
            System.out.println("недостаточно памяти, я перегрелся. Пока!");
            turnOff();
        }
        else {
            System.out.println("Приложение установлено!");
        }
    }
}

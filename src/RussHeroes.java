public class RussHeroes {
    String name;
    String lastName;
    int age;
    boolean perunsPower;

    public void battleCry() {
        String battleCry = String.format("%s кричит: Вперед братья! Накажем этих Ящеров!", name);
        System.out.println(battleCry);
    }

    public void battleWithLizard() {
        if (perunsPower == true) {
            System.out.println(name + " победил проклятого Ящера! Слава Силе Перуна!");
        }
        else {
            String battleResult = String.format("%s %s пал, как герой, в битве под Гипербореей! А ему было всего %d", name, lastName, age);
            System.out.println(battleResult);
        }
    }

    @Override
    public String toString() {
        return "RussHeroes{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", perunsPower=" + perunsPower +
                '}';
    }
}

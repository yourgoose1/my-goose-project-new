package gameutils;

import java.util.Random;

public class RollTheCube {
    static Random random = new Random();

    public static byte rollTheFourCube() { // Данный метод для броска 4 гранного кубика выбирает рандомное число от 0 до 3 и к результату прибавляет 1
        byte rollFour = (byte) (random.nextInt(4) + 1);
        System.out.println(rollFour);
        return rollFour;
    }

    public static byte rollTheSixCube() { // Данный метод для броска 6 гранного кубика выбирает рандомное число от 0 до 5 и к результату прибавляет 1
        byte rollSix = (byte) (random.nextInt(6) + 1);
        System.out.println(rollSix);
        return rollSix;
    }

    public static byte rollTheTwelveCube() { // Данный метод для броска 12 гранного кубика выбирает рандомное число от 0 до 11 и к результату прибавляет 1
        byte rollTwelve = (byte) (random.nextInt(12) + 1);
        System.out.println(rollTwelve);
        return rollTwelve;
    }

    public static byte rollTheTwentyCube() { // Данный метод для броска 20 гранного кубика делает все то же самое,только от 0 до 19
        byte rollTwenty = (byte) (random.nextInt(20) + 1);
        System.out.println(rollTwenty);
        return rollTwenty;
    }

    public static byte rollTheTenCube() { // Диапазон от 0 до 9
        byte rollTen = (byte) (random.nextInt(10));
        System.out.println(rollTen);
        return rollTen;
    }

    public static byte rollTheNextTenCube() { // Диапазон от 0 до 9 и при выводе добавляем 0, чтобы значение было, кау у куба
        byte rollNextTen = (byte) (random.nextInt(10));
        System.out.println(rollNextTen + "0");
        return rollNextTen;
    }
}

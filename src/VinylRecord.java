public class VinylRecord {
    String band;
    String album;
    int release;

    public static int amountOfVinylRecordsProduced = 0;
    public static void incrementVinylRecordProduced() {
        amountOfVinylRecordsProduced++;
    }

    @Override
    public String toString() {
        return "VinylRecord{" +
                "band='" + band + '\'' +
                ", album='" + album + '\'' +
                ", release=" + release +
                '}';
    }

     public VinylRecord (String band, String album, int release) {
        this.band = band;
        this.album = album;
        this.release = release;
     }
}

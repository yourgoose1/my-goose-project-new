public class SmartPhone {
    String os;
    int ram;
    String manufacturer;

    public void makeCall() {
        System.out.println("Бип-бип, Алло?");
    }

    public void turnOff() {
        System.out.println("Пум-пум-пуууууууум");
    }

    AndroidSmartPhone androidSmartPhone = new AndroidSmartPhone();
    AppleSmartPhone appleSmartPhone = new AppleSmartPhone();
   XiaomiAndroidSmartPhone xiaomiAndroidSmartPhone = new XiaomiAndroidSmartPhone();

    SmartPhone[] smartPhones = new SmartPhone[]
            {androidSmartPhone, appleSmartPhone, xiaomiAndroidSmartPhone};

    public void main(String[] args) {
        System.out.println(smartPhones);
    }

}

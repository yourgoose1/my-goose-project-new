package homework6;

public class MyStack<T> {

    T[] elements = (T[]) new Object[3]; //{e,null,null,null}
    int index = 0; //изменил индекс на 0, чтобы не было ошибки при вызове пустого стека

    //класть
    public void push(T element) {
        if (index==elements.length-1) {
            int oldLength = elements.length;
            int newLength = elements.length*2;
            T[] newElements = (T[]) new Object[newLength];
            System.arraycopy(elements, 0,newElements, 0, oldLength);
            elements = newElements;
        }
        index++;
        elements[index] = element;
    }
    //брать
    public T pop()  {
        T element = elements[index];
        elements[index] = null;
        index--;
        return element;
    }
}

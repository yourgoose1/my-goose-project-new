package homework6;

import java.util.Scanner;

public class ForCalculate {

    Scanner scanner = new Scanner(System.in);
    String userEnter = scanner.nextLine().toLowerCase();
    String[] partEnter = userEnter.split(" "); // с пробелом
    int a = Integer.parseInt(partEnter[0]);
    int b = Integer.parseInt(partEnter[2]);
    String operation = partEnter[1];

    public void calculate() {
        try {
            switch (operation) {
                case "+", "плюс", "plus" -> System.out.println(a + b);
                case "-", "минус", "minus" -> System.out.println(a - b);
                case "*", "умножить", "помножить", "multiply" -> System.out.println(a * b);
                case "/", "разделить", "поделить", "divide" -> {
                    if (b == 0) {
                        System.out.println("Алё! Делит на ноль нельзя!");
                    } else System.out.println(a / b);
                }
            }
        } catch (Exception e) {
            System.err.println("Неверный формат введенных данных");
            e.printStackTrace();
        }
    }
}






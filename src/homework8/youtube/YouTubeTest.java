package homework8.youtube;

import java.util.ArrayList;

public class YouTubeTest {
    public static void main(String[] args) {
        Account account = new Account();
        ArrayList<Video> likedVideos = new ArrayList<>();
        likedVideos.add(new Video("PRO100SUPER", "Карп крутится 10 часов вокруг себя", 10000, "Вася Пупкин: Не правда! Тут не 10 часов!!!"));
        likedVideos.add(new Video("Volodya777", "Решение той самой проблемы с компухтером", 3, "AWP_MASTER: Видео можно наполовину уменьшить, если вырезать все Эмммм"));
        likedVideos.add(new Video("BBC", "Касатка снова доказывает, что она умнее человека", 4, "Умный мужик: Это не касатка"));
        account.displayAllLikedVideos(likedVideos);
        account.displayAllVideoByDurationComparable(likedVideos);
        account.displayAllVideoByDurationComparator(likedVideos);
        account.addVideoToFavourite(new Video("Super_Brain", "Пробуем плеснуть воду в раскаленное масло", 12, "Мама: Спасибо, что спалил кухню!"));
        account.removeVideoFromFavourite(likedVideos.get(2));
        account.removeDublicatesFromFavourites();
    }
}

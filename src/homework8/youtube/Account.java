package homework8.youtube;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Account {
    ArrayList<Video> likedVideos = new ArrayList<>();

    @Override
    public String toString() {
        return "Account{" +
                "likedVideos=" + likedVideos +
                '}';
    }

    public void displayAllLikedVideos(ArrayList<Video> likedVideos) {
        System.out.println("Список понравившихся видео:");
        for (Video video : likedVideos) {
            System.out.println(video);
        }
    }

    public void displayAllVideoByDurationComparable(ArrayList<Video> likedVideos) {
        System.out.println("Список понравившихся видео, отсортированный по возврастанию длительности:");
        Collections.sort(likedVideos,(Video o1, Video o2) -> { return o1.compareTo(o2);} );
        for (Video video : likedVideos) {
            System.out.println(video);
        }
    }

    public void displayAllVideoByDurationComparator(ArrayList<Video> likedVideos) {
        System.out.println("Список понравившихся видео, отсортированный по возврастанию длительности:");
        likedVideos.sort(new Comparator<Video>() {
            @Override
            public int compare(Video o1, Video o2) {
                return o1.duration - o2.duration;
            }
        });
        for (Video video : likedVideos) {
            System.out.println(video);
        }
    }

    public void addVideoToFavourite(Video video) {
        likedVideos.add(video);
    }

    public void removeVideoFromFavourite(Video video) {
        likedVideos.remove(video);
    }

    public void removeDublicatesFromFavourites() {
        ArrayList<Video> uniqueVideos = new ArrayList<>(likedVideos);
        uniqueVideos.removeIf(video -> !uniqueVideos.contains(video));
        likedVideos = uniqueVideos;
    }
}

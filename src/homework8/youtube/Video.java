package homework8.youtube;

import java.util.Objects;

public class Video {
    public Video(String author, String name, int duration, String comments) {
        this.author = author;
        this.name = name;
        this.duration = duration;
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Video{" +
                "author='" + author + '\'' +
                ", name='" + name + '\'' +
                ", duration=" + duration +
                ", comments='" + comments + '\'' +
                '}';
    }

    String author;
    String name;
    int duration;
    String comments;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Video video = (Video) o;
        return duration == video.duration && Objects.equals(name, video.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, duration);
    }

    public int compareTo(Video another) {
        return Integer.compare(this.duration, another.duration);
    }
}

package homework8.youtube;

import java.util.HashMap;
import java.util.Map;

public class Translator {
    private final Map<String, String> dictionary;

    public Translator() {
        dictionary = new HashMap<>();
    }

    public void addWord(String wordOnEnglish, String wordOnRussian) {
        this.dictionary.put(wordOnEnglish, wordOnRussian);
    }

    public String translation(String wordOnEnglish) {
        return this.dictionary.getOrDefault(wordOnEnglish, "unknown");
    }

    public void removeWord(String wordOnEnglish) {
        this.dictionary.remove(wordOnEnglish);
    }
}

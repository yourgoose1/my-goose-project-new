package homework8.youtube;

public class TranslatorDemo {
    public static void main(String[] args) {
        Translator translator = new Translator();
        translator.addWord("beaver", "бобёр");//По сути мы можем сразу прописывать несколько значений в wordOnRussian
        translator.addWord("hamster", "хомяк");
        translator.addWord("bear", "медведь");
        translator.addWord("beer", "Пиво");

        System.out.println(translator.translation("beaver"));
        System.out.println(translator.translation("hamster"));
        System.out.println(translator.translation("bear"));
        System.out.println(translator.translation("Brain"));
        translator.removeWord("beer");
    }
}

public class VoronezhMusicShop {
    public static void main(String[] args) {
        MusicShop voronezhMusicShop = new MusicShop();
        voronezhMusicShop.address = "Олимпийский бульвар 12";

        VinylRecord vinyl1 = new VinylRecord("Arctic Monkeys", "Hambug", 2009);
        VinylRecord.incrementVinylRecordProduced();
        VinylRecord vinyl2 = new VinylRecord("The Drums", "Portamento", 2011);
        VinylRecord.incrementVinylRecordProduced();
        VinylRecord vinyl3 = new VinylRecord("Lumen", "Три пути", 2004);
        VinylRecord.incrementVinylRecordProduced();

        MusicShop.addVinylToStorage(vinyl1);
        MusicShop.addVinylToStorage(vinyl2);
        MusicShop.addVinylToStorage(vinyl3);
        MusicShop.showVinylStorage();
        System.out.println(VinylRecord.amountOfVinylRecordsProduced);

    }
}

package homework10;

import java.util.Scanner;

import static org.apache.commons.lang3.StringUtils.reverse;
import static org.apache.commons.lang3.StringUtils.swapCase;

public class StringUtilsDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userEnter = scanner.nextLine();

        System.out.println(reverse(userEnter));
        System.out.println(swapCase(userEnter));
    }
}

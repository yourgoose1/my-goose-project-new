
public class Hyperborea {
    public static void main(String[] args) {
        RussHeroes hero1 = new RussHeroes();
        hero1.name = "Дрочеслав";
        hero1.lastName = "Долгорукий";
        hero1.age = 45;
        hero1.perunsPower = true;

        RussHeroes hero2 = new RussHeroes();
        hero2.name = "Радислав";
        hero2.lastName = "Двухъяблочков";
        hero2.age = 24;
        hero2.perunsPower = false;

        System.out.println(hero1);
        System.out.println(hero2);

        hero2.battleCry();
        hero2.battleWithLizard();
        hero1.battleWithLizard();
    }
}
